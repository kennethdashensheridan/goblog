package main

import (
	"fmt"
	"log"
	"net/http"
	"text/template"
)

const (
	Host = "localhost"
	Port = "8081"
)

type Student struct {
	Name  string
	Title string
	MOS   int
}

func renderTemplate(w http.ResponseWriter, r *http.Request) {
	student := Student{
		Name:  "Ken Sheridan",
		Title: "United States Marine",
		MOS:   6842,
	}
	parsedTemplate, _ := template.ParseFiles("Template/index.html")
	err := parsedTemplate.Execute(w, student)
	if err != nil {
		log.Println("Error executing template :", err)
		return
	}
}

func main() {
	// Print to screen "Welcome to the Go Language"
	fmt.Println("Welcome to the Go Language.")

	// set a variable, and explicitly state the type for the value
	var setType float64 = 1.17

	// set a variable, and automatically inherit the type from the value
	inheritType := 1.17

	// Print value and type
	fmt.Printf("Variable setType is of type %T and holds the value %v\n", setType, setType)

	fmt.Printf("Variable inheritType is of type %T and holds the value %v\n", inheritType, inheritType)

	fileServer := http.FileServer(http.Dir("./resources"))

	// /resources handler
	http.Handle("/resources/", http.StripPrefix("/resources", fileServer))

	// /root handler
	http.HandleFunc("/index", renderTemplate)

	err := http.ListenAndServe(Host+":"+Port, nil)
	if err != nil {
		log.Fatal("Error Starting the HTTP Server :", err)
		return
	}

}
